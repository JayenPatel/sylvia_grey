<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Corporate Laundry | Sylvia Grey Laundry Service | For Businesses in London</title>
    <meta name="description" content="Corporate Laundry from Sylvia Grey Launderers Dry Cleaners, Collection and Delivery. We are proud to offer this service to businesses accross London, call our team today to find out more! +020 8961 0281 & Email: info@sylviagrey.com">
    <meta name="keywords" content="Corporate Laundry, Sylvia Grey, Laundry services, Laundry services London, Best Laundry Service in London, laundromat near me, laundry near me, Collection and Delivery, London,UK">
    <meta name="author" content="rudhisasmito.com">

    <?php include("style.php"); ?>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('img').on('click', function () {
            var image = $(this).attr('src');
            //alert(image);
            $('#myModal').on('show.bs.modal', function () {
                $(".showimage").attr("src", image);
            });
        });
    });
    </script>
</head>

<body>

    <?php include("header.php"); ?>

    <!-- BANNER -->
    <div class="section banner-page margin-bottom-70"
        style="background:url(img/corporate-business-bg.jpg) no-repeat center top;  background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h1 class="title-page">Corporate & Business</h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Corporate & Business</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- ABOUT FEATURE -->

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p class="text-justify">There are a number of cases and conditions applied when someone selects a
                        laundry service. The ones that are able to welcome and work upon special requests are the ones
                        that eventually become comparatively more successful than others, and it results in nothing but
                        the best outcomes all over the board. <strong>Same Day Laundry Service London</strong> is something of a reality,
                        and we’ve taken measures to make it a separate service in its own right. But, doing so the
                        options for the business agreement between every customer and ourselves become quite clear. Not
                        only are you getting your emergency laundry satisfied within the space of a single day, but
                        you’re also getting all the perks and benchmarks that we uphold by ourselves intact too.</p>
                    <div class="margin-bottom-50"></div>
                </div>
                <div class="col-sm-6 col-md-6 margin-bottom-70">
                    <img src="img/corporate-business-banner.jpg" id="4" data-toggle="modal" data-target="#myModal"  alt="">
                </div>
            </div>

        </div>
    </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/same-day-turnaround.jpg" id="1" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Same day turnaround</h2>
                            <h5>7 days a week</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/collection-delivery.jpg" id="2" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Collection & Delivery</h2>
                            <h5>London-wide scheduled service</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/monthly-accounts.jpg" id="3" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Monthly Accounts</h2>
                            <h5>Clear and simple monthly accounts</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div>
                <div class="modal-body">
                    <img class="showimage img-responsive" src="" />
                </div>
            </div>
        </div>
    </div>

    <?php include("footer.php"); ?>

    <?php include("script.php"); ?>


</body>

</html>