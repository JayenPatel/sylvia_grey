<!DOCTYPE html>
<html lang="zxx">

<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
  
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Clean - Cleaning Services HTML Template</title>
    <meta name="description"
        content="Super Clean - Cleaning Services HTML Template. It is built using bootstrap 3.3.2 framework, works totally responsive, easy to customise, well commented codes and seo friendly.">
    <meta name="keywords"
        content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
    <meta name="author" content="rudhisasmito.com">
    <?php include("style.php"); ?>

</head>

<body>

    <?php include("header.php"); ?>

    <div class="section pad">
        <div class="container">
            <div class="row">

                <div class="col-sm-5 col-md-3 col-md-offset-1">
                    <div class="people">
                        <img class="user-pic" src="images/about-img.png" alt="">
                    </div>
                </div>
                <div class="col-sm-7 col-md-7">

                    <div class="margin-bottom-30"></div>
                    <h2 class="title-404">404 PAGE</h2>
                    <h3 class="subtitle-404">Sorry! The page you're looking not found</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                    <a href="index.php" class="btn btn-secondary" title="OUR COMPANY">Back To Home</a>
                    <div class="margin-bottom-50"></div>

                </div>

            </div>
        </div>
    </div>

    <?php include("footer.php"); ?>

    <?php include("script.php"); ?>


</body>


</html>