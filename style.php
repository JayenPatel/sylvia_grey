
    <!-- ==============================================
	Favicons
	=============================================== -->
    <link rel="shortcut icon" href="images/favicon.ico.png">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

    <!-- ==============================================
	CSS VENDOR
	=============================================== -->
    <link rel="stylesheet" type="text/css" href="css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/vendor/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/vendor/simple-line-icons.css">
    <link rel="stylesheet" type="text/css" href="css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/vendor/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="css/vendor/magnific-popup.css">

    <!-- ==============================================
	Custom Stylesheet
	=============================================== -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <meta name="google-site-verification" content="9OCCMCeQCjpj5jYjJU-dg-LlKY5OWj0Bdt62Xv0qTe0" />

    <script type="text/javascript" src="js/vendor/modernizr.min.js"></script>