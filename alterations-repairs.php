<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Alterations &amp; Repairs | Laundry Service | Sylvia Grey Launderers</title>
    <meta name="description" content="Sylvia Greye Specialist Dry cleaners provides professional dry cleaning, laundry, Ironing services for your needs.">
    <meta name="keywords" content="Sylvia Greye, Alterations & Repairs, Dry cleaners,Dry cleaning, Laundry Service, Best Dry Cleaning Service, Laundry Service Near Me, Pick-Up & Delivery, Launderette Near Me">
    <meta name="author" content="rudhisasmito.com">

    <?php include("style.php"); ?>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
    $('img').on('click', function () {
        var image = $(this).attr('src');
        //alert(image);
        $('#myModal').on('show.bs.modal', function () {
            $(".showimage").attr("src", image);
        });
    });
});
    </script>

</head>

<body>

    <?php include("header.php"); ?>

    <!-- BANNER -->
    <div class="section banner-page"
        style="background:url(img/alterations-repairs-bg.jpg) no-repeat center top;  background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h1 class="title-page">Alterations & Repairs</h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Alterations Repairs</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- ABOUT FEATURE -->

    <div class="section pad">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 margin-bottom-30">
                    <img src="img/alterations-repairs-banner.jpg" id="4" data-toggle="modal" data-target="#myModal" alt="">
                </div>
                <div class="col-sm-6 col-md-6">
                    <p class="text-justify">Dry cleaning is perhaps the most recognizable alternative to traditional
                        modes, and the sector has come a long way by creating a lasting impact upon society. We
                        undertake measures that are incredibly standardized according to present times, impress upon the
                        dedication of quality above everything else. Thus, we are regularly touted as the <strong>Best
                            Dry Cleaning Service</strong> out there, but judging such a title would only be possible if
                        you take charge and seek out our services. The results are as pristine and clean as if you’ve
                        bought them for the very first time.</p>
                    <div class="margin-bottom-50"></div>
                </div>

            </div>

        </div>
    </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/sleeve-shortening.jpg" id="1" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Sleeve shortening & waist let in/out</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/shirt-blouse-alterations.jpg" id="2" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Shirt/blouse alterations</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/zip-button-replacement.jpg" id="3" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Zip/button replacement</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div>
                <div class="modal-body">
                    <img class="showimage img-responsive" src="" />
                </div>
            </div>
        </div>
    </div>

    <?php include("footer.php"); ?>

    <?php include("script.php"); ?>


</body>

</html>