<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Household Laundry | Dry Cleaners | Sylvia Grey Launderette</title>
    <meta name="description" content="Sylvia Greye Dry Cleaners specialises in Household Laundry. Enquire online or give us a call now. Lowest rates on all Laundry services. Collection and Delivery available on most of our services.">
    <meta name="keywords" content="Household Laundry,Sylvia Greye Launderers, Best Laundry Service in London, launderette near me, Pick-Up & Delivery, laundry near me, dry cleaners, dry cleaners near me, Dry cleaning services, London">
    <meta name="author" content="rudhisasmito.com">

    <?php include("style.php"); ?>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
    $('img').on('click', function () {
        var image = $(this).attr('src');
        //alert(image);
        $('#myModal').on('show.bs.modal', function () {
            $(".showimage").attr("src", image);
        });
    });
    });
    </script>

</head>

<body>

    <?php include("header.php"); ?>

    <!-- BANNER -->
    <div class="section banner-page margin-bottom-70"
        style="background:url(img/household-bg.jpg) no-repeat center top;  background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h1 class="title-page">Household</h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Household</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- ABOUT FEATURE -->

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p class="text-justify">The entire point of a service mainly focuses upon the conditions under which
                        people may actually
                        seek them out. That’s why <strong>Online Dry Cleaning Service</strong> is so great, as you don’t
                        have to directly
                        involve yourself, and everything shall be done for you. Moreover, the entire case also relates
                        to the
                        fact that you can select all the necessary conditions that you want applied in terms of
                        inclusion to the
                        service. Utmost satisfaction are our central aims, and everything we do creates a sense of
                        entitlement
                        for our clients that’s not easy to replace in any way. You can even track online what has
                        happened with
                        your order quite easily.</p>
                    <div class="margin-bottom-50"></div>
                </div>
                <div class="col-sm-6 col-md-6 margin-bottom-70">
                    <img src="img/household-1.jpg" id="1" data-toggle="modal" data-target="#myModal" alt="">
                </div>
            </div>

        </div>
    </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/shirt-service.jpg" id="2" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Shirt Service</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/dry-cleaning.jpg" id="3" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Dry Cleaning</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/iron-only.jpg" id="4" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Iron only</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div>
                <div class="modal-body">
                    <img class="showimage img-responsive" src="" />
                </div>
            </div>
        </div>
    </div>


    <?php include("footer.php"); ?>

    <?php include("script.php"); ?>


</body>

</html>