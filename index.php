<?php session_start();  ?>
<!DOCTYPE html>
<html lang="zxx">



<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sylvia Grey | Best Laundry Service in London | Sylvia Grey Launderers</title>
    <meta name="description"
        content="Sylvia Grey is a leading laundry service provider company in London, UK. It offers high quality bespoke, hand-finished dry cleaning &amp; laundry services at the best price with same-day laundry service.">
    <meta name="keywords" content="Sylvia Grey Launderers,laundromat near me,Best Laundry Service in London, Best Dry Cleaners London, Online Dry Cleaning Service, Laundry Cleaning Service, Same Day Laundry Service, Laundry Service Near Me, Dry Cleaning Service, Best Dry Cleaners, Pick-Up & Delivery">
    <meta property="og:locale" content="en_GB" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sylvia Grey | Best Laundry Service in London | Sylvia Grey Launderers" />
    <meta property="og:description" content="Sylvia Grey is a leading laundry service provider company in London, UK. It offers high quality bespoke, hand-finished dry cleaning &amp; laundry services at the best price with same-day laundry service." />
    <meta property="og:url" content="https://sylviagrey.com/" />
    <meta property="og:site_name" content="Sylvia Grey | Best Laundry Service in London | Sylvia Grey Launderers" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="Sylvia Grey is a leading laundry service provider company in London, UK. It offers high quality bespoke, hand-finished dry cleaning &amp; laundry services at the best price with same-day laundry service." />
    <meta name="twitter:title" content="Sylvia Grey | Best Laundry Service in London | Sylvia Grey Launderers" />
    <meta name="twitter:site" content="@ShahidDiwan20" />
    <meta name="twitter:creator" content="@ShahidDiwan20" />
    <meta name="author" content="rudhisasmito.com">
    <?php include("style.php"); ?>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        // $("#btn_Submit").click(function(){
        $("#dv_Massage").fadeIn();

        $("#dv_Massage").fadeOut(10000);
        //});
    });
    $(document).ready(function () {
    $('img').on('click', function () {
        var image = $(this).attr('src');
        //alert(image);
        $('#myModal').on('show.bs.modal', function () {
            $(".showimage").attr("src", image);
        });
    });
});
    </script>
        
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143707741-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-143707741-1');
    </script>


</head>

<body>

    <?php include("header.php"); ?>

    <!-- BANNER -->
    <div id="slides" class="section banner banner-height margin-bottom-70">
        <ul class="slides-container">
            <li>
                <img src="img/home-bg.jpg" alt="">
                <div class="overlay-bg"></div>
                <div class="container">
                    <div class="wrap-caption center">
                        <h2 class="caption-heading">
                            London’s most exclusive service offering bespoke, hand-finished dry cleaning and laundry
                        </h2>
                        <a href="contact.php" class="btn btn-primary blue-colors" title="Get in Touch">Get in Touch</a>
                    </div>
                </div>
            </li>
            <li>
                <img src="img/home-bg-1.jpg" alt="">
                <div class="overlay-bg"></div>
                <div class="container">
                    <div class="wrap-caption center">
                        <h2 class="caption-heading">
                            London’s most exclusive service offering bespoke, hand-finished dry cleaning and laundry
                        </h2>
                        <a href="contact.php" class="btn btn-primary blue-colors" title="Get in Touch">Get in Touch</a>
                    </div>
                </div>
            </li>
            <li>
                <img src="img/home-bg-2.jpg" alt="">
                <div class="overlay-bg"></div>
                <div class="container">
                    <div class="wrap-caption center">
                        <h2 class="caption-heading">
                            London’s most exclusive service offering bespoke, hand-finished dry cleaning and laundry
                        </h2>
                        <a href="contact.php" class="btn btn-primary blue-colors" title="Get in Touch">Get in Touch</a>
                    </div>
                </div>
            </li>
        </ul>
        <nav class="slides-navigation">
			<div class="container">
				<a href="#" class="next">
					<i class="fa fa-chevron-right"></i>
				</a>
				<a href="#" class="prev">
					<i class="fa fa-chevron-left"></i>
				</a>
	      	</div>
	    </nav>
    </div>

    <!-- INFO -->
    <div class="section">
        <div class="container">

            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <h3>Best Laundry services in London</h3>
                    <p class="text-justify">Sylvia Grey is the most exclusive online hand-finished dry cleaning and
                        laundry services in
                        London, UK. We offer laundry services to employ credible experts to provide a complete laundry
                        solution at the affordable price with same-day delivery. Our laundry services are hospitality,
                        corporate business, alterations repairs, and household. Book today from our website!</p>
                    <p class="text-justify">When talking about <b>Best Laundry & Cleaning Service</b>, there are lots of
                        questions that come
                        in mind. Most prominent among these are how good the results can be, and we can assure you that
                        our methodologies are extremely effective in terms of both following standards, as well as
                        implementing true innovations wherever it’s possible. You can also rest assured that
                        industry-standard measures are taken to remove stains and discoloration that are not easy to
                        remove. Across the board, the entirety of the service shall take care of your property as if it
                        is our own. The eventual result relates to the ultimate satisfaction with what you’ve entrusted
                        us.</p>
                    <div class="margin-bottom-50"></div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <img src="img/best-laundry-services-in-london.jpg" alt="">
                </div>
            </div>

        </div>
    </div>

    <!-- SERVICES -->
    <div class="section wedo pad">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <!-- BOX 1 -->
                    <div class="box-image-1">
                        <div class="media">
                            <img src="img/banner-1.jpg" id="1" data-toggle="modal" data-target="#myModal" alt=""
                                class="img-responsive">
                        </div>
                        <div class="body">
                            <a href="hospitality.php" class="title">Hospitality</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <!-- BOX 2 -->
                    <div class="box-image-1">
                        <div class="media">
                            <img src="img/banner-2.jpg" id="2" data-toggle="modal" data-target="#myModal" alt=""
                                class="img-responsive">
                        </div>
                        <div class="body">
                            <a href="corporate-business.php" class="title">Corporate & Business</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <!-- BOX 3 -->
                    <div class="box-image-1">
                        <div class="media">
                            <img src="img/banner-3.jpg" id="3" data-toggle="modal" data-target="#myModal" alt=""
                                class="img-responsive">
                        </div>
                        <div class="body">
                            <a href="alterations-repairs.php" class="title">Alterations & Repairs</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <!-- BOX 3 -->
                    <div class="box-image-1">
                        <div class="media">
                            <img src="img/banner-4.jpg" id="4" data-toggle="modal" data-target="#myModal" alt=""
                                class="img-responsive">
                        </div>
                        <div class="body">
                            <a href="household.php" class="title">Household</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div>
                <div class="modal-body">
                    <img class="showimage img-responsive" src="" />
                </div>
            </div>
        </div>
    </div>



    <!-- CONTACT -->
    <div class="section contact parallax100 pad">
        <div class="container">

            <div class="col-sm-12 col-md-12">
                <h2 class="section-heading white">
                    Get in Touch
                </h2>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6 col-md-6">
                <div class="contact-info">
                    <p>We are determined to carry on our mission for as long as there will be a demand for that kind of
                        care!</p>
                    <p><a href="mailto:info@sylviagrey.com">info@sylviagrey.com</a></p>
                    <p class="phone"><i class="fa fa-phone"></i><a href=tel:208-961-0281"> 020 8961 0281</a></p>
                    <p>Working Days: Mon. – Sun. <br>Working Hours: 8.00AM – 5.00PM</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <?php if(isset($_SESSION['mail_send']) && $_SESSION['mail_send']==true ): ?>
                <div id="dv_Massage" class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success">
                            <strong>Success!</strong> Thank you for choosing Sylvia-Gray.
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(isset($_SESSION['mail_send']) && $_SESSION['mail_send']==false ): ?>
                <div id="dv_Massage" class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-warning">
                            <strong>Warning!</strong> Something went wrong.
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(isset($_SESSION['mail_wrong']) && $_SESSION['mail_wrong']!="") :?>
                <div id="dv_Wrong" class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-warning">
                            <strong>Warning!</strong> <?php echo $_SESSION['mail_wrong']; ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div id="error" class="alert alert-danger text-left" style="display:none;">Something went wrong, try
                    after sometime.</div>
                <form action="send_mail.php" method="post" class="form-contact" id="contact-form"
                    data-toggle="validator">
                    <div class="form-group">
                        <input type="text" class="form-control" name="fullname" id="p_name" placeholder="Name*"
                            required="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="p_email" placeholder="Email*"
                            required="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="phone" id="p_phone" placeholder="Phone Number*"
                            pattern="[0-9]{3}[0-9]{3}[0-9]{4}" maxlength="15" onkeypress="return isNumber(event)"
                            required="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <textarea id="p_message" class="form-control" name="message" rows="6"
                            placeholder="Write message*"></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <div id="success"></div>
                        <button type="submit" value="Submit" class="btn btn-primary blue-colors">ASK A QUOTE</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- MAPS -->
    <div class="maps-wraper">
         <div style="width: 100%"><iframe width="100%" height="400" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d310.2810736199247!2d-0.26109619341963464!3d51.52700019431326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876114d67e28231%3A0x80e7a7e9fe6e445e!2sSylvia%20Grey%20Laundries%20Limited!5e0!3m2!1sen!2sin!4v1584429805198!5m2!1sen!2sin" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
        </div>
        <!-- <div id="maps" class="maps" data-lat="51.5267523" data-lng="-0.2631095"
            data-marker="images/cd-icon-location.png">
        </div> -->
    </div>
    
   


    <?php include("footer.php"); ?>

    <?php include("script.php"); ?>
    <script>
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    </script>

</body>

</html>
<?php session_destroy(); ?>