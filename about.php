<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About Sylvia Grey | Laundry Services</title>
    <meta name="description"
        content="Sylvia Grey is a professional hand-finished dry cleaning &amp; laundry company in London, UK. It provides bespoke, hand-finished dry cleaning and laundry service. For more information call us on +020 8961 0281.">
    <meta name="keywords" content="About Us Sylvia Grey, Sylvia Grey Launderers, Laundromat Near Me, Dry Cleaning Service, Laundry Service, best Laundry Service, London, UK">
    <meta name="author" content="rudhisasmito.com">

    <?php include("style.php"); ?>

</head>

<body>

    <?php include("header.php"); ?>

    <!-- BANNER -->
    <div class="section banner-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="title-page">About Us</div>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">About Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- ABOUT FEATURE -->
    <div class="section pad">
        <div class="container">

            <div class="row">
                <div class="col-sm-8 col-md-8">

                    <h3>Setting the Trend since 1985</h3>
                    <p class="lead">In 1985 Sylvia Grey was the first company to introduce same day express service and
                        seven day service to 5 star hotels in Central London. Now, 3 decades later, we continue to push
                        the bar.</p>
                    <div class="margin-bottom-50"></div>
                    <a href="contact.php" class="btn btn-primary blue-colors" title="Learn More">Contact Us</a>
				</div>
				<div class="col-sm-4 col-md-4">
                    <div class="vidimg">
                        <div class="text-center">
                            <div id="fb-root"></div>
                            <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2">
                            </script>
                            <!-- <a class="popup-youtube"
                                href="https://www.facebook.com/sylviagreylaunderers/videos/911586245852465/"><span
                                    class="fa fa-play fa-3x playvid"></span></a> -->
                        </div>
                        <div class="fb-video" data-href="https://www.facebook.com/sylviagreylaunderers/videos/911586245852465/"
                            data-width="100" data-show-text="false">
                         
                        </div>
                        <!-- <img src="img/home-bg.jpg" alt="" class="img-responsive"> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- WHY -->
    <div class="section pad bglight">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h2 class="section-heading">
                        Why choosing us
                    </h2>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="box-icon-3">
                        <div class="infobox">
                            <h5 class="title">Unmatched quality</h5>
                            <div class="text">Our made to order wicker laundry baskets, tailor made for us in Somerset,
                                along with our acid free tissue paper set the standard for quality hand finished,
                                bespoke packaging and presentation to London’s hotel industry.</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="box-icon-3">
                        <div class="infobox">
                            <h5 class="title">As individual as our clients</h5>
                            <div class="text">In the 90’s we set another trend by offering hung garments presented in
                                zip up suit covers with hotel branding.</div>
                            <div class="text">With the drive to reduce carbon footprint, Sylvia Grey is assisting hotels
                                such as The Andaz, Liverpool Street meet their plastic and recycling commitments by
                                replacing nylon suit covers with non-woven ones and using the highest quality wooden
                                hangers sourced from ethical suppliers.</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="box-icon-3">
                        <div class="infobox">
                            <h5 class="title">Wet cleaning</h5>
                            <div class="text">In 2015 we were amongst the first companies to convert our plant to wet
                                cleaning – the healthy and environmentally friendly alternative to dry cleaning with
                                solvents.</div>
                            <div class="text">We are proud to be working with kreussler wet clean water based detergents
                                which are not only environmentally friendly also effective in stain removal but give a
                                fresher and softer feel to garments.</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="box-icon-3">
                        <div class="infobox">
                            <h5 class="title">Exceptional client care</h5>
                            <div class="text">Our non-biological detergents are excellent choices for anyone who suffers
                                with sensitive skin, weather-induced sensitivities, or with existing skin conditions
                                such as eczema that could easily be aggravated.</div>
                            <div class="text">If a client insists on providing their own detergent we neutralise the
                                wash drum prior to washing garments.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MISSION -->
   



    <?php include("footer.php"); ?>

    <?php include("script.php"); ?>


</body>

</html>