	<!-- FOOTER SECTION -->
	<div class="footer">

	    <div class="container">

	        <div class="row">
	            <div class="col-sm-5 col-md-5">
	                <div class="footer-item">
	                    <img src="images/logo-white.png" alt="logo bottom" class="logo-bottom">
	                    <h3 class="margin-bottom-10 text-white">Call Now : <a href=tel:208-961-0281">020 8961 0281</a></h3>
	                    <div class="footer-sosmed">
	                        <a href="https://www.facebook.com/sylviagreylaunderers/" target="_blank" title="">
	                            <div class="item">
	                                <i class="fa fa-facebook"></i>
	                            </div>
	                        </a>
	                        <a href="https://twitter.com/ShahidDiwan20" target="_blank" title="">
	                            <div class="item">
	                                <i class="fa fa-twitter"></i>
	                            </div>
							</a>
							<a href="https://www.linkedin.com/company/sylvia-grey-london/" target="_blank" title="">
	                            <div class="item">
	                                <i class="fa fa-linkedin"></i>
	                            </div>
	                        </a>
	                        <a href="https://www.instagram.com/sylviagreylondon/" target="_blank" title="">
	                            <div class="item">
	                                <i class="fa fa-instagram"></i>
	                            </div>
	                        </a>
	                        <a href="https://www.pinterest.co.uk/shahidsylviagrey/" target="_blank" title="">
	                            <div class="item">
	                                <i class="fa fa-pinterest"></i>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-2 col-md-2 col-md-offset-1">
	                <div class="footer-item">
	                    <div class="footer-title text-white">
	                        Company
	                    </div>
	                    <ul class="list">
	                        <li class="text-white"><a href="index.php" title="">Home</a></li>
	                        <li class="text-white"><a href="about.php" title="">About Us</a></li>
	                        <li class="text-white"><a href="contact.php" title="">Contact</a></li>
	                    </ul>
	                </div>
	            </div>
	            <div class="col-sm-4 col-md-4">
	                <div class="footer-item">
	                    <div class="footer-title text-white">
	                        Services
	                    </div>
	                    <ul class="list">
	                        <li class="text-white"><a href="hospitality.php" title="">Hospitality</a></li>
	                        <li class="text-white"><a href="corporate-business.php" title="">Corporate & Business</a></li>
	                        <li class="text-white"><a href="alterations-repairs.php" title="">Alterations & Repairs</a></li>
	                        <li class="text-white"><a href="household.php" title="">Household</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="fcopy">
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-12 col-md-12">
	                <p class="ftex text-center text-white">&copy; <script type="text/javascript">var year = new Date();document.write(year.getFullYear());</script> Sylvia Grey Launderers - All Rights Reserved</p>
	            </div>
	        </div>
	    </div>
	</div>
	</div>
	<script type="application/ld+json"> { "@context" : "https://schema.org", "@type" : "Organization", "name" : "Sylvia Grey", "url" : "https://sylviagrey.com/", "sameAs" : ["https://www.facebook.com/sylviagreylaunderers/","https://www.pinterest.co.uk/shahidsylviagrey/","https://www.instagram.com/sylviagreylondon/"], "logo": "https://sylviagrey.com/wp-content/uploads/2018/10/Sylvia-Grey-Logo-v1-1024x663.png", "legalName" : "Sylvia Grey", "address":[ { "@type": "PostalAddress", "addressCountry": "UK", "addressLocality": "London", "addressRegion": "Eng", "postalCode": "NW10 6LE", "streetAddress": "23-25 Gorst Road, Park Royal, London,"}], "contactPoint" :[ { "@type" : "ContactPoint", "telephone" : "+44 020 8961 0281", "contactType" : "Customer Service"} ], "aggregateRating": { "@type": "AggregateRating", "ratingValue": "5.0", "reviewCount": "80" } } </script>
	<!-- <script>gtag('event', 'send', { event_category: 'contact us', event_action: 'submit', event_label: 'send message', event_value:'0' });</script> -->