<!-- Load page -->
<div class="animationload">
    <div class="loader"></div>
</div>

<!-- HEADER -->
<div class="header">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-5">
                    <div class="topbar-left">
                        
                    </div>
                </div>
                <div class="col-sm-9 col-md-7">
                    <div class="topbar-right">
                        <ul class="topbar-menu">
                            <li class="text-white"><i class="fa fa-phone"></i> Call Us Today : <a href="tel:208-961-0281">020 8961 0281</a></li>
                            <li class="text-white"><i class="fa fa-envelope"></i> <a href="mailto:info@sylviagrey.com">info@sylviagrey.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- NAVBAR SECTION -->
    <div class="navbar navbar-main">

        <div class="container container-nav">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="index.php">
                    <img src="images/logo.png" alt="" />
                </a>
            </div>


            <nav class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="header_top"> <a href="index.php" class="f-s-16">Home</a> </li>
                    <li class="header_top"> <a href="about.php" class="f-s-16">About Us</a> </li>
                    <li class="dropdown header_top">
                        <a href="hospitality.php" class="dropdown-toggle f-s-16" data-toggle="dropdown"
                            data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="hospitality.php" class="f-s-16">Hospitality</a></li>
                            <li><a href="corporate-business.php" class="f-s-16">Corporate & Business</a></li>
                            <li><a href="alterations-repairs.php" class="f-s-16">Alterations & Repairs</a></li>
                            <li><a href="household.php" class="f-s-16">Household</a></li>
                        </ul>
                    </li>
                    <li class="header_top"> <a href="contact.php" class="f-s-16">Contact Us</a> </li>

                </ul>

            </nav>

        </div>
    </div>
</div>