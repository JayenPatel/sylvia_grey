<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hospitality Laundry Service | Laundry Service Near Me | Sylvia Grey</title>
    <meta name="description" content="It’s very important to keep all kinds of fabric clean if you’re provisioning hospitality. Hospitality Laundry Service provider company in London, UK. dry cleaning & laundry services at the best price with laundry service.">
    <meta name="keywords" content="Hospitality Laundry Service, Sylvia Grey, laundromat near me, laundry near me, Laundry Service, dry cleaning, Best Laundry Wash near me, Laundry services near me, Dry Cleaning Service, Same Day Laundry Service, London, UK ">
    <meta name="author" content="rudhisasmito.com">

    <?php include("style.php"); ?>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
    $('img').on('click', function () {
        var image = $(this).attr('src');
        //alert(image);
        $('#myModal').on('show.bs.modal', function () {
            $(".showimage").attr("src", image);
        });
    });
});
    </script>

</head>

<body>

    <?php include("header.php"); ?>

    <!-- BANNER -->
    <div class="section banner-page margin-bottom-70"
        style="background:url(img/hospitality-bg.jpg) no-repeat center top;  background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h1 class="title-page">Hospitality</h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Hospitality</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="section pad">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 margin-bottom-30">
                    <img src="img/hospitality_banner.jpg" id="4" data-toggle="modal" data-target="#myModal" alt="">
                </div>
                <div class="col-sm-6 col-md-6">
                    <h2 class="header_title">The Very Best Answer to your Question ‘Best Laundry Wash near me’</h2>
                    <p class="text-justify">It’s very important to keep all kinds of fabric clean if you’re provisioning hospitality. If you’re looking for an answer to ‘Who can wash clothes near me?’, then it means that you’re lacking in resources. Fear not as many professional services are actually available that provide such services on request. However, Sylvia Grey is an agency that professionally handles all forms of dry cleaning, and they’ve got resources to work with you can’t definitely imagine. This means they can handle all kinds of fabric you’ve got on a regular basis. With a service charge that is totally satisfactory, there’s no reason whatsoever why you shouldn’t choose this option of ‘laundry services near me’. Additionally, Sylvia Grey has a wide range of options wherein you can request them to clean your bedlinen, table linen, towels, workwear and even stuff from your washroom. Truly, in hospitality Laundry services, the expectation from such pieces of fabric are quite high, and therefore, you need the best option always.</p>
                    <div class="margin-bottom-50"></div>
                </div>

            </div>

        </div>
    </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/bedlinen.jpg" id="1" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Bedlinen</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/tablelinen.jpg" id="2" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Tablelinen</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/towelling.jpg" id="3" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Towelling</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/workwear.jpg" id="4" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Workwear</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="box-image-2">
                        <div class="media">
                            <img src="img/washroom.jpg" id="5" data-toggle="modal" data-target="#myModal" alt="rud" class="img-responsive">
                        </div>
                        <div class="body text-center-p">
                            <h2 class="title">Washroom</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div>
                <div class="modal-body">
                    <img class="showimage img-responsive" src="" />
                </div>
            </div>
        </div>
    </div>

    <?php include("footer.php"); ?>

    <?php include("script.php"); ?>


</body>

</html>